from abc import ABC, abstractmethod


class YandexUser(ABC):
    def __init__(self, first_name, last_name, app='Yandex'):
        self.first_name = first_name
        self.last_name = last_name
        self.app = app

    def __repr__(self):
        return f'{self.first_name} {self.last_name}'

    @abstractmethod
    def open(self):
        pass


class User(YandexUser):
    def __init__(self, first_name, last_name, app):
        super(User, self).__init__(first_name, last_name, app)

    def open(self):
        print("Open user app")


user1 = User("Poghos", "Poghosyan", "Yandex app")
print(user1)

# print(user1.first_name + '-' + user1.last_name +" "+ user1.open +" "+ user1.app + " mijocov")


class Driver(YandexUser):
    def __init__(self, first_name, last_name, app):
        super(Driver, self).__init__(first_name, last_name, app)

    def open(self):
        print("Open driveri app")
# driver1 = Driver("Vagharshak","Inqstinqyan","Yandex app","Maps","hastatyum e")


# print(driver1.first_name + '-' + driver1.last_name +" "+ driver1.open +" "+ driver1.app + " mijocov "
# + driver1.chek + " kanch@")