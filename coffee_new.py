class Coffee:
    """This class to coffee"""

    def __init__(self, temperature, cup):
        self.cup = cup
        self.temperature = temperature
        self.milk = False
        self.sugar = 0

    def add_milk(self):
        self.milk = True

    def add_sugar(self, count):
        self.sugar = self.sugar + count


moca = Coffee(temperature='10C', cup='big')
cappuccino = Coffee(temperature='30C', cup='middle')



print(moca.milk)
moca.add_milk()
print(moca.milk)

print(moca.sugar)
moca.add_sugar(count=5)
print(moca.sugar)



