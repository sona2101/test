import os


def print_path(path):
    files = os.listdir(path)
    print(files)

    for file in files:
        full_path = os.path.join(path, file)

        if os.path.isdir(full_path):
            print_path(path=full_path)

        else:
            print(full_path)


my_path = 'E:\Trainer Vibes'

print_path(path=my_path)



