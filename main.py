# this is a guess the number game

from random import randint


def game():
    guesses_taken = 0
    guess = None

    print('Բարև, կնշե՞ս քո անունը։')
    my_name = input()

    number = randint(1, 20)
    print(f'Թույն, {my_name}, Ես թիվ եմ մտապահել 1-20։')

    while guesses_taken < 6 and guess != number:
        guesses_taken = guesses_taken + 1

        print('Փորձի՛ր գուշակել')
        guess = input()
        guess = int(guess)

        if guess < number:
            print('Չէ-չէ-չէ, քիչ է')
        elif guess > number:
            print('չէէէէէէ, մեծ է')
        else:
            guesses_taken = str(guesses_taken)
            print(f'Ընտիր, {my_name} դու գուշակեցիր {guesses_taken} -րդ փորձից։')

    if guess != number:
        print(f'Սորրի, ես պահել էի այս թիվը՝ {number}')


while True:
    want_to_play = input("Do you want to play my game? Yes or No \n")
    if want_to_play == 'Yes':
       game()
    else:
        print("Բա ինչ ես Run անում, վաաաայ")
        break

