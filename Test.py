def tower_of_hanoi(n, start, end):
    if n == 1:
        print_fromt_to(start, end)
    else:
        other = 6 - (start + end)

        tower_of_hanoi(n-1, start, other)
        print_fromt_to(start, end)
        tower_of_hanoi(n-1, other, end)

def print_fromt_to(where_from, to):
    print(f'mode disk from {where_from} to {to}')

n = 3
tower_of_hanoi(n, 1, 3)

