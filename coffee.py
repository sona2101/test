class Coffee():
    """coffee making automat"""

    def __init__(self, type, temperature, cup):
        """using some ingredients"""
        self.type = type
        self.temperature = temperature
        self.cup = cup
        print('Coffee in process...')

    def milk(self):
       '''if the customer wants coffee with the milk'''
        print(self.type.title() + "coffee with the milk")

    def sugar(self):
       '''if the customer wants coffee with the sugar'''
        print(self.type.title() + "coffee with the sugar")

my_coffee = Coffee('Moca', '28C', 'Big Size')

print(my_coffee.temperature)